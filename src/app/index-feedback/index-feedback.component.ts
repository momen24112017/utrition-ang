import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index-feedback',
  templateUrl: './index-feedback.component.html',
  styleUrls: ['./index-feedback.component.css']
})
export class IndexFeedbackComponent implements OnInit {
  imgSrc;
  constructor() { }

  ngOnInit(): void {
  }
  imgModal(str){
    this.imgSrc = str;
  }

}
