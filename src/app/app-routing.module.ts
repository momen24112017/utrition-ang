import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { FreqAqComponent } from './freq-aq/freq-aq.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { MyMealPlanComponent } from './my-meal-plan/my-meal-plan.component';
import { PlanDetailsComponent } from './plan-details/plan-details.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { RegLoginComponent } from './reg-login/reg-login.component';
import { SuccessVerifyEmailRegComponent } from './success-verify-email-reg/success-verify-email-reg.component';
import { SuccessfulPlanPurchaseComponent } from './successful-plan-purchase/successful-plan-purchase.component';
import { TakeQuestionnaireComponent } from './take-questionnaire/take-questionnaire.component';
import { VerifyEmailRegComponent } from './verify-email-reg/verify-email-reg.component';
import { ProfileComponent } from './profile/profile.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ForgetPassEmailComponent } from './forget-pass-email/forget-pass-email.component';
import { ForgetPassNewpassComponent } from './forget-pass-newpass/forget-pass-newpass.component';
import { SendEmailRecoverPassComponent } from './send-email-recover-pass/send-email-recover-pass.component';
import { UserDataComponent } from './user-data/user-data.component';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    {
      path: '',
      component: IndexComponent
    },
   
    {
      path: 'reg-login',
      component: RegLoginComponent
    },
    {
      path: 'forget-pass-email',
      component: ForgetPassEmailComponent
    },
    {
    path: 'forget-pass-newpass',
      component: ForgetPassNewpassComponent
    },
    
   
    {
      path: 'freq-aq',
      component: FreqAqComponent
    },
    {
      path: 'profile',
      component: ProfileComponent
    },
    {
      path: 'gallery',
      component: GalleryComponent
    },
    {
      path: 'verify-email-reg',
      component: VerifyEmailRegComponent
    },
    {
      path: 'send-email-recover-pass',
      component: SendEmailRecoverPassComponent
    },
    {
      path: 'success-verify-email-reg',
      component: SuccessVerifyEmailRegComponent
    },

    {
      path: 'successful-plan-purchase',
      component: SuccessfulPlanPurchaseComponent
    },
   
    {
      path: 'contact-us',
      component: ContactUsComponent
    },
    {
      path: 'check-out',
      component: CheckOutComponent
    },
    {
      path: 'my-meal-plan',
      component: MyMealPlanComponent
    },
    {
      path: 'plan-details/:id',
      component: PlanDetailsComponent
    },
    {
      path: 'user-data/:id',
      component: UserDataComponent
    },

    {
      path: 'take-questionnaire',
      component: TakeQuestionnaireComponent
    },
    {
      path: 'questionnaire',
      component: QuestionnaireComponent
    },
  ], { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
