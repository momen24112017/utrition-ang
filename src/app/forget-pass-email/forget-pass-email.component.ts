import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forget-pass-email',
  templateUrl: './forget-pass-email.component.html',
  styleUrls: ['./forget-pass-email.component.css']
})
export class ForgetPassEmailComponent implements OnInit {

  constructor(private router:Router,private utritionService: UtritionService, private toastr: ToastrService) { }
  
  
  resetPassSpn = false;

  @ViewChild('resetEmail') resetEmailForm: NgForm;
  resetEmail = {email:""};

  resetPassword(){
    this.resetPassSpn = true;
    this.utritionService.checkEmail(this.resetEmail.email).subscribe(res=>{
      if(res.status.code == 5017){
        this.resetPassSpn = false;
        this.toastr.error('Email not found', 'Error', {timeOut: 5000,});
        return;
      }
      if(res.status.code == 200){
        this.resetPassSpn = false;
        this.toastr.success('', 'Success', {timeOut: 5000,});
        localStorage.setItem('utritionUserData',JSON.stringify(res.data));
        //window.location.href="/send-email-recover-pass";
        this.router.navigate(['/send-email-recover-pass'])

      }
    })
  }

  ngOnInit(): void {

  }

}
