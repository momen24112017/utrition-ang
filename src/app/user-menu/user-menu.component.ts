import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css'],
  providers:[DatePipe]
})
export class UserMenuComponent implements OnInit {

  constructor(private router:Router,private utritionService: UtritionService, private datePipe: DatePipe, private toastr: ToastrService) { }

  user: any;
  userProfile: any;
  userAddress: any;
  freezePlan: any;
  Date = new Date();
  medicalMessage: string;
  allergyMessage: string;
  PlanAss: any;
  
  // arrSubmitPlan: any;
  arrMenuUser:any;
  showLoadingSpinForSbMenu = true;
  planAssLength: any;
 
  objFreezePlan = {from:"",to:"",user_id:""};
  freezeSpn = false;
  maxDate: any;
  minDate: any;
  @ViewChild('objFreezePlan') freezePlanForm: NgForm;

  freezingPlan(){
    this.freezeSpn = true;
    this.objFreezePlan.user_id = this.user.id;
    this.objFreezePlan.from = this.freezePlanForm.value.from;
    this.objFreezePlan.to = this.freezePlanForm.value.to;
    if(this.objFreezePlan.from >= this.objFreezePlan.to){
      this.freezeSpn = false;
      this.toastr.error('Set Dates Correctly', 'Error', {timeOut: 5000,});
      return;
    }
    this.utritionService.freezePlan(this.objFreezePlan).subscribe(res=>{
      if(res.status.code == 200){
        this.freezeSpn= false;
        this.toastr.success('Plan Freeze', 'Success', {timeOut: 5000,});
        //window.location.href="/my-meal-plan"
        this.router.navigate(['/my-meal-plan']) 
      }
    })
  }
  
  addAddress(){
    this.userAddress.push({'address_name':'','address':''});
  }

  listUserData(){
    this.showLoadingSpinForSbMenu = true;
    this.utritionService.listUserData(this.user.id).subscribe(res=>{
      this.showLoadingSpinForSbMenu = false;
      this.userProfile = res.user;
      this.userAddress = res.user_address;
      this.freezePlan = res.freeze_plan;
      this.PlanAss = res.plan_ass;

      var currentDate = this.datePipe.transform(this.Date, 'yyyy-MM-dd');
      if(this.freezePlan != null){
        if(currentDate > this.freezePlan.to){
          this.freezePlan = null;
        }
      }

      if(this.userAddress.length == 0){
        this.addAddress();
      }

      var arrMenuUsertest;
      this.planAssLength = this.PlanAss.length
      this.PlanAss.forEach(function (value){
        if(value.menu_submit == undefined){
          arrMenuUsertest = value.menuUser;
        }
        // else{
        //   this.arrSubmitPlan = value.menu_submit;
        // }
      });
      this.arrMenuUser = arrMenuUsertest;

    });

  }

  ngOnInit(): void {
    
    this.user = this.utritionService.getuserData();
    this.listUserData();
    var date  = new Date();
    this.minDate = date.setDate( date.getDate() + 2 );
    this.minDate = this.datePipe.transform(date, 'yyyy-MM-dd');
    this.maxDate = date.setDate( date.getDate() + 47 );
    this.maxDate = this.datePipe.transform(date, 'yyyy-MM-dd');

  }

}
