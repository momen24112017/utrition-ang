import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexStaticComponent } from './index-static.component';

describe('IndexStaticComponent', () => {
  let component: IndexStaticComponent;
  let fixture: ComponentFixture<IndexStaticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexStaticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexStaticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
