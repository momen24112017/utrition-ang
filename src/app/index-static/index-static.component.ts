import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index-static',
  templateUrl: './index-static.component.html',
  styleUrls: ['./index-static.component.css']
})
export class IndexStaticComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  opensignup(){
    window.open('https://www.goldsgym.ae/transform2.0/')
    // window.location.href="https://www.goldsgym.ae/transform2.0/"
  }
}
