import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss'],
  providers: [DatePipe]
})
export class CheckOutComponent implements OnInit {

  constructor(private cd: ChangeDetectorRef, private route: ActivatedRoute, private router: Router, private utritionService: UtritionService, private datePipe: DatePipe, private toastr: ToastrService) { }

  plan: any = {
    id: '',
    name: '',
    price: '',
    short_desc: '',
    long_desc: '',
    banner_img: '',
    feature_img: '',
    price_per_day: '',
    price_per_month: '',
    feature: '',
    long_feature: '',
    menu_sample: '',
    color: '',
    paln_table: '',
    created_at: '',
    updated_at: '',
    priceAfterAdding_5: '',
    vat: '',
    days: ''

  }
  user: any;
  date = new Date();
  startDate: any;
  minDate: any;
  applySpn = false;
  CheckoutSpn = false;
  promocodeValue = "";
  usingValueofPromoCode: any;
  promocode: any;

  additionalPlanDetails = [
    {
      id: '1',
      three_meal: {
        days: '20',
        price: '2700',
        priceAfterAdding_5: '2835',
        vat:'135'
      },
      tow_meals: {
        days: '20',
        price: '2443',
        priceAfterAdding_5: '2565',
        vat:'122'
      }
    },

    {
      id: '2',
      three_meal: {
        days: '20',
        price: '2700',
        priceAfterAdding_5: '2835',
        vat:'135'
      },
      tow_meals: {
        days: '20',
        price: '2443',
        priceAfterAdding_5: '2565',
        vat:'122'
      }
    },
    {
      id: '3',
      three_meal: {
        days: '20',
        price: '2957',
        priceAfterAdding_5: '3105',
        vat:'148'
      },
      tow_meals: {
        days: '20',
        price: '2700',
        priceAfterAdding_5: '2835',
        vat:'135'
      }
    },
    {
      id: '5',
      three_meal: {
        days: '14',
        price: '1809',
        priceAfterAdding_5: '1899',
        vat:'90'
      },
      tow_meals: undefined
    }
  ]

  detailsObj = {
    id: '',
    three_meal: {
      days: '',
      price: '',
      priceAfterAdding_5: ''
    },
    tow_meals: {
      days: '',
      price: '',
      priceAfterAdding_5: ''
    }
  }
  tempPlan;
  // tempPlan={
  //   id: '',
  //   name: '',
  //   price: '',
  //   short_desc: '',
  //   long_desc: '',
  //   banner_img: '',
  //   feature_img: '',
  //   price_per_day: '',
  //   price_per_month: '',
  //   feature: '',
  //   long_feature: '',
  //   menu_sample: '',
  //   color: '',
  //   paln_table: '',
  //   created_at: '',
  //   updated_at: '',
  //   priceAfterAdding_5: '',
  //   vat: '',
  //   days: ''

  // };
  ngOnInit(): void {
    this.route.queryParams.subscribe((res: any) => {
      this.plan = res;
      console.table(this.plan);
    })
    for (let index = 0; index < this.additionalPlanDetails.length; index++) {
      if (this.additionalPlanDetails[index].id == this.plan.id) {
        this.detailsObj = this.additionalPlanDetails[index];
        this.tempPlan = this.additionalPlanDetails[index].three_meal;
        console.log('this is temp plan',this.tempPlan);
        
      }
    }

    this.user = this.utritionService.getuserData();
    var datePlusTwoDays = this.date.setHours(this.date.getHours() + 48);
    this.minDate = this.datePipe.transform(datePlusTwoDays, 'yyyy-MM-dd')
    this.startDate = this.datePipe.transform(datePlusTwoDays, 'yyyy-MM-dd')
  }

  checkPromocode() {
    this.applySpn = true;
    if (this.promocodeValue == "") {
      this.applySpn = false;
      this.toastr.error('Enter Promo code', 'Error', { timeOut: 5000, });
      return;
    }
    this.utritionService.checkPromocode(this.promocodeValue).subscribe(res => {
      if (res.status.code == 200) {
        this.applySpn = false;
        this.toastr.success('Promo code is Valid', 'Success', { timeOut: 5000, });
        this.promocode = res.promocode;
        this.usingValueofPromoCode = res.promocode.value
      } else {
        this.applySpn = false;
        this.toastr.error('Invalid Promo code', 'Error', { timeOut: 5000, });
        this.promocode = 0;
      }
    })
  }

  checkout() {
    this.CheckoutSpn = true;
    var toDate = this.datePipe.transform(this.date.setDate(this.date.getDate() + 30), 'yyyy-MM-dd');
    var total: any = this.tempPlan.price;
    if (this.promocode != undefined) {
      total = this.tempPlan.priceAfterAdding_5 - this.promocode.value;
      var objCheckout = {
        "user_id": this.user.id,
        "plan_id": this.plan.id,
        "promocode_id": this.promocode.id,
        "from": this.startDate,
        "price": total,
        "to": toDate
      };
    } else {
      total = this.tempPlan.priceAfterAdding_5;
      objCheckout = {
        "user_id": this.user.id,
        "plan_id": this.plan.id,
        "promocode_id": 0,
        "from": this.startDate,
        "price": total,
        "to": toDate
      };
    }

    this.utritionService.checkout(objCheckout).subscribe(res => {
      this.CheckoutSpn = false;
      if (res.status.code == 200) {
        this.toastr.success('You bought a plan successfully', 'Success', { timeOut: 5000, });
        window.location.href = res.rootPay;
        //this.router.navigate(['/successful-plan-purchase'])  
      } else {
        this.CheckoutSpn = false;
        this.toastr.error(res.status.message, 'Error', { timeOut: 5000, });
      }
    });
  }
  changeMeal(number) {
    if (number == 3) {
      // this.plan.price = this.detailsObj.three_meal.price;
      // this.plan.priceAfterAdding_5 = this.detailsObj.three_meal.priceAfterAdding_5;
      this.tempPlan = { ...this.detailsObj.three_meal }
    } else if (number == 2) {
      // this.plan.price = this.detailsObj.tow_meals.price;
      // this.plan.priceAfterAdding_5 = this.detailsObj.tow_meals.priceAfterAdding_5;
      this.tempPlan = { ...this.detailsObj.tow_meals }

    }
    console.table('plan', this.plan);

    this.cd.detectChanges();
  }

}
