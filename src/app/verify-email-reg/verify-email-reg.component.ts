import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-verify-email-reg',
  templateUrl: './verify-email-reg.component.html',
  styleUrls: ['./verify-email-reg.component.css']
})
export class VerifyEmailRegComponent implements OnInit {

  constructor(private utritionService: UtritionService, private toastr: ToastrService) { }

  resendMailSpn= false;
  user: any;

  resendMail(){
    this.resendMailSpn=true;
    console.log('user',this.user);
    if(this.user != undefined){
      this.utritionService.resendMail(this.user.id).subscribe(res=>{  
        console.log('result',res);
        if(res.status.code == 200){
          
          this.resendMailSpn=false;
          this.toastr.success('Mail sent successfully', 'Success', {timeOut: 5000,});
        }else{
          this.resendMailSpn = false;
          this.toastr.error('Sorry, try again', 'Error', {timeOut: 5000,});
        }
      });
    }
  }

  ngOnInit(): void {

    this.user = this.utritionService.getuserData();

  }

}
