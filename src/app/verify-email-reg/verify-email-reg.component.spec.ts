import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyEmailRegComponent } from './verify-email-reg.component';

describe('VerifyEmailRegComponent', () => {
  let component: VerifyEmailRegComponent;
  let fixture: ComponentFixture<VerifyEmailRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyEmailRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyEmailRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
