import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtritionService } from '../services/utrition.service';

@Component({
  selector: 'app-plan-details',
  templateUrl: './plan-details.component.html',
  styleUrls: ['./plan-details.component.css']
})
export class PlanDetailsComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService, private router: Router, private utritionService: UtritionService, private route: ActivatedRoute) {

  }
  index: any;
  plan: any = {
    banner_img: "",
    color: '',
    created_at: '',
    feature: [],
    feature_img: '',
    id: '',
    long_desc: '',
    long_feature: [],
    menu_sample: '',
    name: '',
    paln_table: '',
    price: '',
    price_per_day: '',
    price_per_month: '',
    short_desc: '',
    updated_at: '',
    priceAfterAdding_5: '',
    vat: '',
  }
  user: any;
  plan_id: any;
  plan_details: any;
  plans: any[] = [];
  percentage_5: number;
  priceAfterAdding_5;


  //plan details prices..
  additionalPlansDetails = [
    {
      id: 1,
      three_meal: {
        days: '20',
        meal_price: '2700',
      },
      tow_meals: {
        days: '20',
        mealprice: '2443'
      }
    },

    {
      id: 2,
      three_meal: {
        days: '20',
        meal_price: '2700',
      },
      tow_meals: {
        days: '20',
        mealprice: '2443'
      }
    },
    {
      id: 3,
      three_meal: {
        days: '20',
        meal_price: '2957',
      },
      tow_meals: {
        days: '20',
        mealprice: '2700'
      }
    },
    {
      id: 5,
      three_meal: {
        days: '14',
        meal_price: '1809',
      },
      tow_meals: undefined
    }
  ];
  additionSingledetails;
  ngOnInit(): void {
    window.scrollTo(0, 0)
    console.log('this is id', this.route.snapshot.paramMap.get('id'));
    this.plan_id = this.route.snapshot.paramMap.get('id');
    for (let index = 0; index < this.additionalPlansDetails.length; index++) {
      if (this.additionalPlansDetails[index].id == this.plan_id) {
        this.additionSingledetails = this.additionalPlansDetails[index];
      }
    }
    this.spinner.show();
    this.utritionService.listPlan().subscribe(res => {
      for (let index = 0; index < res.data.length; index++) {
        if (res.data[index].id == this.plan_id) {
          this.plan = res.data[index];
          parseInt(this.plan.price)
          this.percentage_5 = (this.plan.price * 5) / 100;
          this.plan.priceAfterAdding_5 = this.plan.price + this.percentage_5;
          this.plan.vat = this.percentage_5;
        } else {
          console.log('not found ');
        }
      }
      this.spinner.hide();
    }, err => {
      this.spinner.hide();

    })
    // this.plan = JSON.parse(localStorage.getItem('planDetail'));
    this.user = this.utritionService.getuserData();

  }
  checkOut() {
    console.log('que', this.plan);

    this.router.navigate(['/check-out'], { queryParams: this.plan })
  }

}
