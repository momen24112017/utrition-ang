import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';

@Component({
  selector: 'app-reg-login',
  templateUrl: './reg-login.component.html',
  styleUrls: ['./reg-login.component.css']
})
export class RegLoginComponent implements OnInit {

  constructor(private router:Router,private utritionService: UtritionService, private toastr: ToastrService, private spinner: NgxSpinnerService) { }

  @ViewChild('objRegister') registerForm: NgForm;
  @ViewChild('objLogin') loginForm: NgForm;
  objLogin = {email:"",password:""};
  objRegister = {name:"", email:"", password:"",confirm_password:"", age:"", phone:"", title:"mr", address:""};
  regSpn = false;
  loginSpn = false;

  login(){
    this.loginSpn = true;
    this.objLogin.email = this.loginForm.value.email;
    this.objLogin.password = this.loginForm.value.password;
    this.utritionService.login(this.objLogin).subscribe(res=>{
      if(res.status.code == 4030){
        this.loginSpn = false;
        this.toastr.error('Invalid Email', 'Error', {timeOut: 5000,});
        return;
      }
      if(res.status.code == 5013){
        this.loginSpn = false;
        this.toastr.error('Wrong Password', 'Error', {timeOut: 5000,});
        return;
      }
      if(res.status.code == 200){
        this.loginSpn = false;
        this.toastr.success('', 'Success', {timeOut: 5000,});
        localStorage.removeItem('userEmail');
        localStorage.setItem('utritionUserData',this.utritionService.encrypt(JSON.stringify(res.data)));
        if(res.data.first_time == 0){
          // window.location.href="/take-questionnaire";
          this.router.navigate(['/take-questionnaire'])

        }else{
          // window.location.href="/";
          this.router.navigate(['/'])
        }
      }
    })
  }

  register(form: NgForm){

    this.regSpn = true;
    this.objRegister.name = this.registerForm.value.name;
    this.objRegister.email = this.registerForm.value.email;
    this.objRegister.password = this.registerForm.value.password;
    this.objRegister.confirm_password = this.registerForm.value.confirm_password;
    this.objRegister.age = this.registerForm.value.age;
    this.objRegister.phone = this.registerForm.value.phone;
    this.objRegister.address = this.registerForm.value.address;
    if(this.objRegister.password != this.objRegister.confirm_password){
      this.regSpn = false;
      this.toastr.error('Password not matching', 'Error', {timeOut: 5000,});
      return;
    }
    this.utritionService.register(this.objRegister).subscribe(res=>{
      if(res.status.code == 4016){
        this.regSpn = false;
        this.toastr.error('This email is already exist', 'Error', {timeOut: 5000,});
        return;
      }
      if(res.status.code == 200){
        this.regSpn = false;
        this.toastr.success('', 'Success', {timeOut: 5000,});
        localStorage.setItem('userEmail',JSON.stringify(res.data.email));
        // window.location.href="/verify-email-reg";
        this.router.navigate(['/verify-email-reg'])
      }else{
        this.regSpn = false;
        this.toastr.error('', 'Error', {timeOut: 5000,});
      }

    })

  }

  ngOnInit(): void {
  }

}
