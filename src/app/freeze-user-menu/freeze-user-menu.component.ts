import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-freeze-user-menu',
  templateUrl: './freeze-user-menu.component.html',
  styleUrls: ['./freeze-user-menu.component.css'],
  providers:[DatePipe]
})
export class FreezeUserMenuComponent implements OnInit {

  constructor(private router:Router,private utritionService: UtritionService, private datePipe: DatePipe, private toastr: ToastrService) { }

  user: any;
  objFreezePlan = {from:"",to:"",user_id:""};
  freezeSpn = false;
  maxDate: any;
  minDate: any;
  @ViewChild('objFreezePlan') freezePlanForm: NgForm;
  

  freezingPlan(){
    this.freezeSpn = true;
    this.objFreezePlan.user_id = this.user.id;
    this.objFreezePlan.from = this.freezePlanForm.value.from;
    this.objFreezePlan.to = this.freezePlanForm.value.to;
    if(this.objFreezePlan.from >= this.objFreezePlan.to){
      this.freezeSpn = false;
      this.toastr.error('Set Dates Correctly', 'Error', {timeOut: 5000,});
      return;
    }
    this.utritionService.freezePlan(this.objFreezePlan).subscribe(res=>{
      if(res.status.code == 200){
        this.freezeSpn= false;
        this.toastr.success('Plan Freeze', 'Success', {timeOut: 5000,});
        //window.location.href="/my-meal-plan"
        this.router.navigate(['/my-meal-plan'])
      }
    })
  }

  ngOnInit(): void {

    this.user = this.utritionService.getuserData();
    var date  = new Date();
    this.minDate = date.setDate( date.getDate() + 2 );
    this.minDate = this.datePipe.transform(date, 'yyyy-MM-dd');
    this.maxDate = date.setDate( date.getDate() + 47 );
    this.maxDate = this.datePipe.transform(date, 'yyyy-MM-dd');

  }

}
