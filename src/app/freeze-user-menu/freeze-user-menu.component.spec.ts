import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreezeUserMenuComponent } from './freeze-user-menu.component';

describe('FreezeUserMenuComponent', () => {
  let component: FreezeUserMenuComponent;
  let fixture: ComponentFixture<FreezeUserMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreezeUserMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreezeUserMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
