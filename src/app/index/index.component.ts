import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.detectIphone()
  }
  detectIphone() {
    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    // Get the device pixel ratio
    var ratio = window.devicePixelRatio || 1;
    // Define the users device screen dimensions
    var screen = {
      width: window.screen.width * ratio,
      height: window.screen.height * ratio
    };
    // iPhone X Detection
    if (iOS && screen.width == 1125 && screen.height === 2436) {
      console.log('this is iphone');
      
      document.body.classList.add('x-padding');

    } else {
      console.log('it is not iphone');
      document.body.classList.add('body-padding');

    }
  }

}
