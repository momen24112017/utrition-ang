import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreqAqComponent } from './freq-aq.component';

describe('FreqAqComponent', () => {
  let component: FreqAqComponent;
  let fixture: ComponentFixture<FreqAqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreqAqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreqAqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
