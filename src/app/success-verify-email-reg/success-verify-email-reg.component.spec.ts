import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessVerifyEmailRegComponent } from './success-verify-email-reg.component';

describe('SuccessVerifyEmailRegComponent', () => {
  let component: SuccessVerifyEmailRegComponent;
  let fixture: ComponentFixture<SuccessVerifyEmailRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessVerifyEmailRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessVerifyEmailRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
