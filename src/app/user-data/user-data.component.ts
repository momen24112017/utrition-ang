import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { DatePipe } from '@angular/common';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.scss']
})
export class UserDataComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService,private utritionService: UtritionService, private datePipe: DatePipe, private activatedroute: ActivatedRoute) { 
    this.spinner.show();
  }
  user: any;
  userProfile: any;
  userAddress: any;
  freezePlan: any;
  Date = new Date();
  medicalMessage: string;
  allergyMessage: string;
  PlanAss: any;

  // arrSubmitPlan: any;
  arrMenuUser: any;
  showLoadingSpinForSbMenu = true;
  planAssLength: any;

  addAddress() {
    this.userAddress.push({ 'address_name': '', 'address': '' });
  }

  listUserData() {
    this.showLoadingSpinForSbMenu = true;
    // console.log('this is user will listed',this.user);
    this.utritionService.listUserData(this.user_id).subscribe(res => {
    this.spinner.hide();
      
      this.user = res.user
      this.userAddress = res.user_address;
      // console.log('data with user id ',res);
      // console.log('this is user address',this.userAddress);
      // console.log('this is user address from res',res.user_address);
      this.foodAllergy = res.user_q[1].a;
      this.showLoadingSpinForSbMenu = false;
      this.userProfile = res.user;
      this.freezePlan = res.freeze_plan;
      this.PlanAss = res.plan_ass;
      if(res.user_q != null){

        this.user_q = res.user_q[0];
        this.medicalCondition = res.user_q[0].a;
      }
      this.formateMedicalCondition(this.medicalCondition, this.foodAllergy)

      var currentDate = this.datePipe.transform(this.Date, 'yyyy-MM-dd');
      if (this.freezePlan != null) {
        if (currentDate > this.freezePlan.to) {
          this.freezePlan = null;
        }
      }

      if (this.userAddress.length == 0) {
        this.addAddress();
      }

      var arrMenuUsertest;
      this.planAssLength = this.PlanAss.length
      this.PlanAss.forEach(function (value) {
        if (value.menu_submit == undefined) {
          arrMenuUsertest = value.menuUser;
        }
        // else{
        //   this.arrSubmitPlan = value.menu_submit;
        // }
      });
      this.arrMenuUser = arrMenuUsertest;

    },err=>{
      // console.log('this is error',err);
      
    });

  }
  formateMedicalCondition(medical: any, foodAllergy: any) {
    // console.log(medical);
    var selectedMedical = Object.keys(medical).filter(function (key) {
      return medical[key] === true;
    });
    this.medicalcondition = selectedMedical

    var selectedFood = Object.keys(foodAllergy).filter(function (key) {
      return foodAllergy[key] === true;
    });
    this.foodAllergyArr = selectedFood;
  }
  user_id;
  foodAllergy;
  user_q = null;
  ngOnInit(): void {

    this.user_id = this.activatedroute.snapshot.paramMap.get('id');
    // console.log('this is user id ',this.user_id);

    
    // this.user = this.utritionService.getuserData();
    // console.log('this.user',this.user);
    
    this.listUserData();

  }





  // user data 
  @ViewChild('objUser') objUserForm: NgForm;
  objUser = { age: "", name: "", email: "", phone: "", password: "", arrAddress: [] };
  medicalCondition = { Anemia: "", Diabetes: "", BloodPressure: "", Thyroid: "", heart: "", Gastrointestinal: "", otherCondition: "" };
  medicalcondition: any[] = [];
  foodAllergyArr: any = [];

}
