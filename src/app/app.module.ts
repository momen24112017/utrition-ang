import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { UtritionService } from './services/utrition.service';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule, DatePipe } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainNavbarComponent } from './main-navbar/main-navbar.component';
import { SecNavbarComponent } from './sec-navbar/sec-navbar.component';
import { FooterComponent } from './footer/footer.component';
import { IndexComponent } from './index/index.component';
import { IndexStaticComponent } from './index-static/index-static.component';
import { IndexPlansComponent } from './index-plans/index-plans.component';
import { IndexFeedbackComponent } from './index-feedback/index-feedback.component';
import { RegLoginComponent } from './reg-login/reg-login.component';
import { VerifyEmailRegComponent } from './verify-email-reg/verify-email-reg.component';
import { TakeQuestionnaireComponent } from './take-questionnaire/take-questionnaire.component';
import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { ForgetPassEmailComponent } from './forget-pass-email/forget-pass-email.component';
import { ForgetPassNewpassComponent } from './forget-pass-newpass/forget-pass-newpass.component';
import { SuccessVerifyEmailRegComponent } from './success-verify-email-reg/success-verify-email-reg.component';
import { SuccessfulPlanPurchaseComponent } from './successful-plan-purchase/successful-plan-purchase.component';
import { PlanDetailsComponent } from './plan-details/plan-details.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FreqAqComponent } from './freq-aq/freq-aq.component';
import { MyMealPlanComponent } from './my-meal-plan/my-meal-plan.component';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { RenewUserMenuComponent } from './renew-user-menu/renew-user-menu.component';
import { SubmitUserMenuComponent } from './submit-user-menu/submit-user-menu.component';
import { FreezeUserMenuComponent } from './freeze-user-menu/freeze-user-menu.component';
import { ProfileComponent } from './profile/profile.component';
import { GalleryComponent } from './gallery/gallery.component';
import { WeekOneTabComponent } from './week-one-tab/week-one-tab.component';
import { WeekComponent } from './week/week.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SendEmailRecoverPassComponent } from './send-email-recover-pass/send-email-recover-pass.component';
import { UserDataComponent } from './user-data/user-data.component';


@NgModule({
  declarations: [
    AppComponent,
    MainNavbarComponent,
    SecNavbarComponent,
    FooterComponent,
    IndexComponent,
    IndexStaticComponent,
    IndexPlansComponent,
    IndexFeedbackComponent,
    RegLoginComponent,
    VerifyEmailRegComponent,
    TakeQuestionnaireComponent,
    QuestionnaireComponent,
    ForgetPassEmailComponent,
    ForgetPassNewpassComponent,
    SuccessVerifyEmailRegComponent,
    SuccessfulPlanPurchaseComponent,
    PlanDetailsComponent,
    CheckOutComponent,
    ContactUsComponent,
    FreqAqComponent,
    MyMealPlanComponent,
    UserMenuComponent,
    RenewUserMenuComponent,
    SubmitUserMenuComponent,
    FreezeUserMenuComponent,
    ProfileComponent,
    GalleryComponent,
    WeekOneTabComponent,
    WeekComponent,
    SendEmailRecoverPassComponent,
    UserDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    NgbModule
  ],
  providers: [
    UtritionService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
