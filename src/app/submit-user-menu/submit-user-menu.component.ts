import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-submit-user-menu',
  templateUrl: './submit-user-menu.component.html',
  styleUrls: ['./submit-user-menu.component.css'],
  providers:[DatePipe]
})
export class SubmitUserMenuComponent implements OnInit {

  constructor(private router:Router,private utritionService: UtritionService, private datePipe: DatePipe, private toastr: ToastrService) { }

  user: any;
  userProfile: any;
  userAddress: any;
  freezePlan: any;
  Date = new Date();
  medicalMessage: string;
  allergyMessage: string;
  PlanAss: any;
  
  arrSubmitPlan: any;
  arrMenuUser:any;
  showLoadingSpinForSbMenu = true;
  public arrSubmitMenu : Array<object> = [];
  saveMenuSpn = false;
  validationLength = 0;
  @ViewChild('objSubmitMenu') objSubmitMenuForm: NgForm;

  addAddress(){
    this.userAddress.push({'address_name':'','address':''});
  }

  listUserData(){
    this.utritionService.listUserData(this.user.id).subscribe(res=>{
      this.showLoadingSpinForSbMenu = false;
      this.userProfile = res.user;
      this.userAddress = res.user_address;
      this.freezePlan = res.freeze_plan;
      this.PlanAss = res.plan_ass;
      
      var currentDate = this.datePipe.transform(this.Date, 'yyyy-MM-dd');
      if(this.freezePlan != null){
        if(currentDate > this.freezePlan.to){
          this.freezePlan = null;
        }
      }

      if(this.userAddress.length == 0){
        this.addAddress();
      }

      var arrMenuUsertest;
      var arrSubmitPlantest;
      this.PlanAss.forEach(function (value){
        if(value.menu_submit == undefined){
          arrMenuUsertest = value.menuUser;
        }else{
          arrSubmitPlantest = value.menu_submit;
        }
      });
      this.arrMenuUser = arrMenuUsertest;
      if(arrSubmitPlantest != undefined){
        this.arrSubmitPlan = arrSubmitPlantest;
        var arrSubmitPlanLength: any[] = Object.values(this.arrSubmitPlan);
        for(var i = 0; i < arrSubmitPlanLength.length; i++){
          this.validationLength = this.validationLength + arrSubmitPlanLength[i].length;
        }
        this.validationLength = this.validationLength * 4;
      }
    });

  }

  submit(){
    this.saveMenuSpn = true;
    var arrMeals = Object.values(this.objSubmitMenuForm.value);
    if(arrMeals.length != this.validationLength){
      this.saveMenuSpn = false;
      this.toastr.error('There is missing choice please check your choices again', 'Error', {timeOut: 5000,});
      return;
    }
    for(var key = 0; key < arrMeals.length; key++){
      var objSubmitMenu = {breakfast:"", lunch:"", snack:"", dinner:"", day:"", date:""};
      var arrDayDateBreakfast = arrMeals[key];
      arrDayDateBreakfast = (arrDayDateBreakfast as string).split("+");
      objSubmitMenu.day = arrDayDateBreakfast[1];
      objSubmitMenu.date = arrDayDateBreakfast[2];
      objSubmitMenu.breakfast = arrDayDateBreakfast[0];
      key = key + 1;
      objSubmitMenu.lunch = arrMeals[key]as string;
      key = key + 1;
      objSubmitMenu.snack = arrMeals[key]as string;
      key = key + 1;
      objSubmitMenu.dinner = arrMeals[key]as string;
      this.arrSubmitMenu.push(objSubmitMenu);
    }
    this.utritionService.submitMenu(this.arrSubmitMenu, this.user.id).subscribe(res=>{
      if(res.status.code == 200){
        this.saveMenuSpn = false;
        this.toastr.success('Menu submitted successfully', 'Success', {timeOut: 5000,});
        //window.location.href="/my-meal-plan"  
        this.router.navigate(['/my-meal-plan'])          
      }else{
        this.saveMenuSpn = false;
        this.toastr.error('Failed to Submit Menu try again', 'Error', {timeOut: 5000,});
      }
    });
  }


  ngOnInit(): void {

    this.user = this.utritionService.getuserData();
    this.listUserData();

  }

}
