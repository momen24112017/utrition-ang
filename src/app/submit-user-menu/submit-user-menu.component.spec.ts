import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitUserMenuComponent } from './submit-user-menu.component';

describe('SubmitUserMenuComponent', () => {
  let component: SubmitUserMenuComponent;
  let fixture: ComponentFixture<SubmitUserMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitUserMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitUserMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
