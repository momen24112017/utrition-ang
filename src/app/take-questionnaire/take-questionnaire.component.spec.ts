import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakeQuestionnaireComponent } from './take-questionnaire.component';

describe('TakeQuestionnaireComponent', () => {
  let component: TakeQuestionnaireComponent;
  let fixture: ComponentFixture<TakeQuestionnaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakeQuestionnaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakeQuestionnaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
