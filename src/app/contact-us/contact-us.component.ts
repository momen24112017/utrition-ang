import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  constructor(private router:Router,private utritionService: UtritionService, private toastr: ToastrService) { }

  contactUsSpn = false;
  @ViewChild('objContactUs') ContactUsForm: NgForm;

  objContactUs = {first_name:"", last_name:"", email:"", phone:"", message:""};

  contactUs(){
    this.contactUsSpn = true;
    this.objContactUs.first_name = this.ContactUsForm.value.first_name;
    this.objContactUs.last_name = this.ContactUsForm.value.last_name;
    this.objContactUs.email = this.ContactUsForm.value.email;
    this.objContactUs.phone = this.ContactUsForm.value.phone;
    this.objContactUs.message = this.ContactUsForm.value.message;
    this.utritionService.contactUs(this.objContactUs).subscribe(res=>{
      if(res.status.code == 200){
        this.contactUsSpn = false;
        this.toastr.success('Message sent successfully', 'Success', {timeOut: 5000,});
        this.router.navigate(['/'])
      }else{
        this.contactUsSpn = false;
        this.toastr.error('Sorry, try again', 'Error', {timeOut: 5000,});
      }
    });
  }

  ngOnInit(): void {

  }

}
