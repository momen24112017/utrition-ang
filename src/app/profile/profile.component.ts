import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [DatePipe]
})
export class ProfileComponent implements OnInit {

  constructor(private router: Router, private utritionService: UtritionService, private datePipe: DatePipe, private toastr: ToastrService) { }

  @ViewChild('objUser') objUserForm: NgForm;
  objUser = { age: "", name: "", email: "", phone: "", password: "", arrAddress: [] };

  user: any;
  userProfile: any;
  userAddress:any[] = [];
  freezePlan: any;
  Date = new Date();
  medicalMessage: string;
  allergyMessage: string;
  PlanAss: any;
  medicalCondition = { Anemia: "", Diabetes: "", BloodPressure: "", Thyroid: "", heart: "", Gastrointestinal: "", otherCondition: "" };
  foodAllergy = { eggs: "", Dairy: "", BloodPressure: "", Gluten: "", nuts: "", fish: "", Seashell: "", otherAllergy: "" };
  arrMenuUser: any;
  arrSubmitPlan: any;
  spnUpdate = false;
  showLoadingSpinForSbMenu = false;
  ngOnInit(): void {

    this.user = this.utritionService.getuserData();
    this.listUserData();



  }
  listUserData() {
    this.showLoadingSpinForSbMenu = true;
    console.log('will listed',this.user);
    
    this.utritionService.listUserData(this.user.id).subscribe(res => {
      console.log('userdata aa',res);
      
      this.showLoadingSpinForSbMenu = false;
      this.userProfile = res.user;
      this.userAddress = res.user_address;
      console.log('user address', this.userAddress);
      this.freezePlan = res.freeze_plan;
      this.PlanAss = res.plan_ass;
      this.medicalCondition = res.user_q[0].a;
      this.foodAllergy = res.user_q[1].a;
      this.formateMedicalCondition(this.medicalCondition, this.foodAllergy)
      var currentDate = this.datePipe.transform(this.Date, 'yyyy-MM-dd');

      if (this.freezePlan != null) {
        if (currentDate > this.freezePlan.to) {
          this.freezePlan = null;
        }
      }

      if (this.userAddress.length == 0) {
        this.addAddress('', '');
      }

      if (this.medicalCondition.Anemia == null && this.medicalCondition.Diabetes == null && this.medicalCondition.BloodPressure == null
        && this.medicalCondition.Thyroid == null && this.medicalCondition.heart == null && this.medicalCondition.Gastrointestinal == null
        && this.medicalCondition.otherCondition == null) {
        this.medicalMessage = "you don't have any medical condition";
      }
      if (this.foodAllergy.eggs == null && this.foodAllergy.Dairy == null && this.foodAllergy.Gluten == null
        && this.foodAllergy.nuts == null && this.foodAllergy.fish == null && this.foodAllergy.Seashell == null
        && this.foodAllergy.otherAllergy == null) {
        this.allergyMessage = "you don't have any food causes allergy";
      }

      // this.PlanAss.forEach(function (value){
      //   if(value.menu_submit == undefined){
      //     this.arrMenuUser = value.menuUser;
      //   }else{
      //     this.arrSubmitPlan = value.menu_submit;
      //   }
      // });

    });

  }
  addAddress(index, obj) {
    this.userAddress.push({
      'address': "",
      'address_name': "",
      'created_at': "",
      'id': '',
      'updated_at': "",
      'user_id': ''
    })
    console.log(this.userAddress, obj);
    this.userAddress[index].user_id = this.user.user.id;
    this.user.arrAddress = this.userAddress;

  }
  loggg() {
    console.log(this.userAddress);

  }
  removeAddress(index) {
    this.userAddress.splice(index, 1);
  }

  updateUserProfile() {
    var spnUpdate = true;
    this.objUser.name = this.user.name;
    this.objUser.email = this.user.email;
    this.objUser.phone = this.user.phone;
    if (this.user.password != "") {
      this.objUser.password = this.user.password;
    }
    this.objUser.arrAddress = this.userAddress;
    this.objUser.age = "14"
    console.log(this.objUser);
    if (this.objUser.arrAddress.length > 1) {
      for (let i = 0; i < this.objUser.arrAddress.length; i++) {
        if (this.objUser.arrAddress[i].address_name == "" && this.objUser.arrAddress[i].address == "") {
          this.objUser.arrAddress.splice(i, 1);
        }
      }

    } else if (this.objUser.arrAddress.length == 1 && (this.objUser.arrAddress[0].user_address == "" || this.objUser.arrAddress[0].address == "")) {
      this.toastr.warning("You can not remove your only address")
    }
    this.utritionService.updateUserProfile(this.objUser, this.user.id).subscribe(res => {
      console.log(res);
      this.toastr.success('Profile update successfully!', 'Success');
    }, err => {
      console.log(err);
      this.toastr.error('Failed to update profile try again!', 'Error', { timeOut: 5000, });
    })
  }
  medicalcondition: any[] = [];
  foodAllergyArr: any = [];
  formateMedicalCondition(medical: any, foodAllergy: any) {
    console.log(medical);
    var selectedMedical = Object.keys(medical).filter(function (key) {
      return medical[key] === true;
    });
    this.medicalcondition = selectedMedical

    var selectedFood = Object.keys(foodAllergy).filter(function (key) {
      return foodAllergy[key] === true;
    });
    this.foodAllergyArr = selectedFood;
  }
  logout() {
    localStorage.clear();
    this.router.navigate(['/'])
  }


}
