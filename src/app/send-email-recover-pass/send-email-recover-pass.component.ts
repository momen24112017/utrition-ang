import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-send-email-recover-pass',
  templateUrl: './send-email-recover-pass.component.html',
  styleUrls: ['./send-email-recover-pass.component.css']
})
export class SendEmailRecoverPassComponent implements OnInit {

  constructor(private utritionService: UtritionService, private toastr: ToastrService) { }

  ResetPasswordSpn = false;
  user: any;

  resendMailforResetPassword(){
    this.ResetPasswordSpn = true;
    this.utritionService.resendMailforResetPassword(this.user).subscribe(res=>{       
      if(res.status.code == 200){
        this.toastr.success('Mail sent successfully', 'Success', {timeOut: 5000,});
        this.ResetPasswordSpn = false;
      } 
    });
  }


  ngOnInit(): void {

    this.user = localStorage.getItem('utritionUserData');
    this.user = this.user.slice(1, -1);
  
  }

}
