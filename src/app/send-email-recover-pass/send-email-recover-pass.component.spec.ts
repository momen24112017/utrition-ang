import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendEmailRecoverPassComponent } from './send-email-recover-pass.component';

describe('SendEmailRecoverPassComponent', () => {
  let component: SendEmailRecoverPassComponent;
  let fixture: ComponentFixture<SendEmailRecoverPassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendEmailRecoverPassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendEmailRecoverPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
