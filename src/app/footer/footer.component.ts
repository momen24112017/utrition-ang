import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(private utritionService: UtritionService) { }

  public user: any;

  ngOnInit(): void {

    this.user = this.utritionService.getuserData();

  }

}
