import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.css']
})
export class QuestionnaireComponent implements OnInit {

  constructor(private utritionService: UtritionService, private toastr: ToastrService) { }

  questionnaireSpn = false;
  @ViewChild('objQuestionnaire') QuestionnaireForm: NgForm;
  objQuestionnaire = {
    datebirth:"", gender:"", weight:"", height:"", percenatgebodyfat:"", targetweight:"",
    mobile:"", email:"", club:"", agreeTermsofService:"", message:"",
    medicalCondition:{Diabetes:"", BloodPressure:"", Thyroid:"", heart:"", Anemia:"", Gastrointestinal:"", otherCondition:""},
    foodAllergy:{eggs:"", Dairy:"", Gluten:"", nuts:"", fish:"", Seashell:"", otherAllergy:""}
  };

  user: any;

  userQuestionnaire(){
    this.questionnaireSpn = true;
    this.objQuestionnaire.datebirth = this.QuestionnaireForm.value.datebirth;
    this.objQuestionnaire.gender = this.QuestionnaireForm.value.gender;
    this.objQuestionnaire.weight = this.QuestionnaireForm.value.weight;
    this.objQuestionnaire.height = this.QuestionnaireForm.value.height;
    this.objQuestionnaire.percenatgebodyfat = this.QuestionnaireForm.value.percenatgebodyfat;
    this.objQuestionnaire.targetweight = this.QuestionnaireForm.value.targetweight;
    this.objQuestionnaire.mobile = this.QuestionnaireForm.value.mobile;
    this.objQuestionnaire.email = this.QuestionnaireForm.value.email;
    this.objQuestionnaire.club = this.QuestionnaireForm.value.club;
    this.objQuestionnaire.agreeTermsofService = this.QuestionnaireForm.value.agreeTermsofService;
    this.objQuestionnaire.message = this.QuestionnaireForm.value.message;
    this.objQuestionnaire.medicalCondition.Diabetes = this.QuestionnaireForm.value.Diabetes;
    this.objQuestionnaire.medicalCondition.BloodPressure = this.QuestionnaireForm.value.BloodPressure;
    this.objQuestionnaire.medicalCondition.Thyroid = this.QuestionnaireForm.value.Thyroid;
    this.objQuestionnaire.medicalCondition.heart = this.QuestionnaireForm.value.heart;
    this.objQuestionnaire.medicalCondition.Anemia = this.QuestionnaireForm.value.Anemia;
    this.objQuestionnaire.medicalCondition.Gastrointestinal = this.QuestionnaireForm.value.Gastrointestinal;
    this.objQuestionnaire.medicalCondition.otherCondition = this.QuestionnaireForm.value.otherCondition;
    this.objQuestionnaire.foodAllergy.eggs = this.QuestionnaireForm.value.eggs;
    this.objQuestionnaire.foodAllergy.Dairy = this.QuestionnaireForm.value.Dairy;
    this.objQuestionnaire.foodAllergy.Gluten = this.QuestionnaireForm.value.Gluten;
    this.objQuestionnaire.foodAllergy.nuts = this.QuestionnaireForm.value.nuts;
    this.objQuestionnaire.foodAllergy.fish = this.QuestionnaireForm.value.fish;
    this.objQuestionnaire.foodAllergy.Seashell = this.QuestionnaireForm.value.Seashell;
    this.objQuestionnaire.foodAllergy.otherAllergy = this.QuestionnaireForm.value.otherAllergy;
    this.utritionService.userQuestionnaire(this.objQuestionnaire,this.user.id).subscribe(res=>{
      if(res.status.code==200){
        this.questionnaireSpn = false;
        this.toastr.success('Questionnaire sent successfully', 'Success', {timeOut: 5000,});
      }
      window.location.href="/";
    });
  }

  ngOnInit(): void {
    this.user = this.utritionService.getuserData();
  }

}
