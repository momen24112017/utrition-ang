import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexPlansComponent } from './index-plans.component';

describe('IndexPlansComponent', () => {
  let component: IndexPlansComponent;
  let fixture: ComponentFixture<IndexPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
