import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-index-plans',
  templateUrl: './index-plans.component.html',
  styleUrls: ['./index-plans.component.scss']
})
export class IndexPlansComponent implements OnInit {

  constructor(private utritionService: UtritionService, private router: Router) { }

  public arrPlan: any;

  getPlanDetail(plan,index){
    // console.log('plan',plan);
    // localStorage.setItem('planDetail',JSON.stringify(plan));
    // this.router.navigateByUrl('/plan-details');
    this.router.navigate(['plan-details',index]);

  }

  ngOnInit(): void {
    this.utritionService.listPlan().subscribe(res=>{
      this.arrPlan=res.data;
      console.log('res');
    },ero=>{
      console.log('ero');
    })
  }

}
