import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  constructor() { }
  images = [
    'assets/images/gallery/18.jpg',
    'assets/images/gallery/17.jpg',
    'assets/images/gallery/16.jpg',
    'assets/images/gallery/14.jpg',
    'assets/images/gallery/15.jpg',
    'assets/images/gallery/13.jpg',
    'assets/images/gallery/12.jpg',
    'assets/images/gallery/11.jpg',
    'assets/images/gallery/10.jpg',
    'assets/images/gallery/9.jpg',
    'assets/images/gallery/8.jpg',
    'assets/images/gallery/7.jpg',
    'assets/images/gallery/6.jpg',
    'assets/images/gallery/5.jpg',
    'assets/images/gallery/4.jpg',
    'assets/images/gallery/3.jpg',
    'assets/images/gallery/2.jpg',
    'assets/images/gallery/19.jpg',
    'assets/images/gallery/20.jpg',
    'assets/images/gallery/21.jpg']
  ngOnInit(): void {
  }

}
