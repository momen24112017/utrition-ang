import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import * as CryptoJS from 'crypto-js';
import { environment } from '../../environments/environment.prod';
// import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtritionService {
  
  encrypt(value : string) : string{
    return CryptoJS.AES.encrypt(value, 'SecretKeyForEncryption&Descryption:GC3m').toString();
  }
  decrypt(textToDecrypt : string){
    return CryptoJS.AES.decrypt(textToDecrypt, 'SecretKeyForEncryption&Descryption:GC3m').toString(CryptoJS.enc.Utf8);
  }

  userDecrypt: any;
  user: any;
  getuserData(){
    if(localStorage.getItem('utritionUserData')){
      this.userDecrypt  = this.decrypt(localStorage.getItem('utritionUserData'));
      this.user = JSON.parse(this.userDecrypt);
      return this.user;
    }
  }
  
  constructor(private http: HttpClient) { }

  private API_URL= environment.API_URL;
  // private API_URL_TEST = environmentTest.API_URL;

  register(obj): Observable<any> {
    return this.http.post(this.API_URL+'/register', obj);
  }

  login(obj): Observable<any> {
    return this.http.post(this.API_URL+'/login', obj);
  }

  listUserData(user_id): Observable<any> {
    return this.http.get(this.API_URL+'/getUserData/'+user_id);
  }

  submitMenu(arr, user_id): Observable<any> {
    return this.http.post(this.API_URL+'/storeUserPlan/'+user_id, arr);
  }

  updateUserProfile(obj, user_id): Observable<any> {
    return this.http.post(this.API_URL+'/editUserData/'+user_id, obj);
  }

  updateIsFirstTime(user_id): Observable<any> {
    return this.http.get(this.API_URL+'/updateFirstTime/'+user_id);
  }

  resetPassword(obj, email): Observable<any> {
    return this.http.post(this.API_URL+'/resetPassword/'+email, obj);
  }

  contactUs(obj): Observable<any> {
    return this.http.post(this.API_URL+'/contactUs', obj);
  }

  userQuestionnaire(obj, id): Observable<any> {
    return this.http.post(this.API_URL+'/userQuestionnaire/'+id, obj);
  }

  freezePlan(obj): Observable<any> {
    return this.http.post(this.API_URL+'/freezePlan', obj);
  }

  showUserProfile(): Observable<any> {
    return this.http.get(this.API_URL+'/showUserProfile');
  }

  listPlan(): Observable<any> {
    return this.http.get(this.API_URL+'/listPlan');
  }

  resendMail(id): Observable<any> {
    return this.http.get(this.API_URL+'/resendMail/'+id);
  }

  resendMailforResetPassword(email): Observable<any> {
    return this.http.get(this.API_URL+'/resendMailforResetPassword/'+email);
  }

  checkPromocode(promocode): Observable<any> {
    return this.http.get(this.API_URL+'/checkPromocode/'+promocode);
  }

  checkout(obj): Observable<any> {
    return this.http.post(this.API_URL+'/buyPlan', obj);
  }

  checkEmail(email): Observable<any> {
    return this.http.get(this.API_URL+'/checkEmail/'+email);
  }

  checkBankPaymentStatus(user_id):Observable<any> {
    return this.http.post(this.API_URL+'/checkBankPaymentStatus',{'user_id':user_id});
  }

}
