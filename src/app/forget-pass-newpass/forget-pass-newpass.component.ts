import { Component, OnInit, ViewChild } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forget-pass-newpass',
  templateUrl: './forget-pass-newpass.component.html',
  styleUrls: ['./forget-pass-newpass.component.css']
})
export class ForgetPassNewpassComponent implements OnInit {

  constructor(private router:Router,private utritionService: UtritionService, private toastr: ToastrService) { }

  resetSpn = false;
  user: any;
  objPassword = {password:"", confirm_password:""};

  @ViewChild('objPassword') PasswordForm: NgForm;

  ResetPassword(){
    this.objPassword.password = this.PasswordForm.value.password;
    this.objPassword.confirm_password = this.PasswordForm.value.confirm_password;
    this.resetSpn = true;
    if(this.objPassword.password != this.objPassword.confirm_password){
      this.toastr.error('Password not matching', 'Error', {timeOut: 5000,});
      this.resetSpn = false;
      return;
    }
    this.utritionService.resetPassword(this.objPassword, this.user).subscribe(res=>{
      if (res.status.code == 5013){
        this.toastr.error(res.status.message, 'Error', {timeOut: 5000,});
        this.resetSpn = false; 
      }
      if(res.status.code == 5018){
        this.toastr.error(res.status.message, 'Error', {timeOut: 5000,});
        this.resetSpn = false;
      }
      if(res.status.code == 200){
        //window.location.href="/reg-login";
        this.router.navigate(['/reg-login'])
      }
    });
  }
  ngOnInit(): void {

    this.user = localStorage.getItem('utritionUserData');
    this.user = this.user.slice(1, -1);

  }

}
