import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgetPassNewpassComponent } from './forget-pass-newpass.component';

describe('ForgetPassNewpassComponent', () => {
  let component: ForgetPassNewpassComponent;
  let fixture: ComponentFixture<ForgetPassNewpassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgetPassNewpassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgetPassNewpassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
