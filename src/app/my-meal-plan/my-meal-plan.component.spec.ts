import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyMealPlanComponent } from './my-meal-plan.component';

describe('MyMealPlanComponent', () => {
  let component: MyMealPlanComponent;
  let fixture: ComponentFixture<MyMealPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyMealPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyMealPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
