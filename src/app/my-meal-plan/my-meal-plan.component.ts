import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-my-meal-plan',
  templateUrl: './my-meal-plan.component.html',
  styleUrls: ['./my-meal-plan.component.css'],
  providers:[DatePipe]
})
export class MyMealPlanComponent implements OnInit {

  constructor(private utritionService: UtritionService, private datePipe: DatePipe) { }

  user: any;
  userProfile: any;
  userAddress: any;
  freezePlan: any;
  Date = new Date();
  medicalMessage: string;
  allergyMessage: string;
  PlanAss: any;
  
  // arrSubmitPlan: any;
  arrMenuUser:any;
  showLoadingSpinForSbMenu = true;
  planAssLength: any;

  addAddress(){
    this.userAddress.push({'address_name':'','address':''});
  }

  listUserData(){
    this.showLoadingSpinForSbMenu = true;
    this.utritionService.listUserData(this.user.id).subscribe(res=>{
      this.showLoadingSpinForSbMenu = false;
      this.userProfile = res.user;
      this.userAddress = res.user_address;
      this.freezePlan = res.freeze_plan;
      this.PlanAss = res.plan_ass;
     
      var currentDate = this.datePipe.transform(this.Date, 'yyyy-MM-dd');
      if(this.freezePlan != null){
        if(currentDate > this.freezePlan.to){
          this.freezePlan = null;
        }
      }

      if(this.userAddress.length == 0){
        this.addAddress();
      }

      var arrMenuUsertest;
      this.planAssLength = this.PlanAss.length
      this.PlanAss.forEach(function (value){
        if(value.menu_submit == undefined){
          arrMenuUsertest = value.menuUser;
        }
        // else{
        //   this.arrSubmitPlan = value.menu_submit;
        // }
      });
      this.arrMenuUser = arrMenuUsertest;

    });

  }

  ngOnInit(): void {

    this.user = this.utritionService.getuserData();
    this.listUserData();

  }

}
