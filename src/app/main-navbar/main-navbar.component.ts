import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';


@Component({
  selector: 'app-main-navbar',
  templateUrl: './main-navbar.component.html',
  styleUrls: ['./main-navbar.component.scss']
})
export class MainNavbarComponent implements OnInit {

  constructor(private router: Router,private utritionService: UtritionService, private toastr: ToastrService, private spinner: NgxSpinnerService) { }

  public user: any;

  logout(){
    this.spinner.show("logoutSpinner");
    localStorage.removeItem('utritionUserData');
    localStorage.removeItem('userEmail');
    localStorage.clear();
    this.spinner.hide("loginSpinner");
    //window.location.href='/';
    this.router.navigate(['/'])
    
  }

 

  ngOnInit(): void {
    this.user = this.utritionService.getuserData();
    
  }
  navigateToMealplan(){
    console.log('hero');
    
    this.router.navigate(['/'])
    setTimeout(() => {
      document.getElementById("indexplan").scrollIntoView();
    }, 500);
  }
  about(){
    console.log('hero');
    
    this.router.navigate(['/'])
    setTimeout(() => {
      document.getElementById("about").scrollIntoView();
    }, 500);
  }

}
