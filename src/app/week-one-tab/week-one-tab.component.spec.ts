import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekOneTabComponent } from './week-one-tab.component';

describe('WeekOneTabComponent', () => {
  let component: WeekOneTabComponent;
  let fixture: ComponentFixture<WeekOneTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeekOneTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeekOneTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
