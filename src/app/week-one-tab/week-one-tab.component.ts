import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-week-one-tab',
  templateUrl: './week-one-tab.component.html',
  styleUrls: ['./week-one-tab.component.css'],
  providers:[DatePipe]
})
export class WeekOneTabComponent implements OnInit {

  constructor(private utritionService: UtritionService, private datePipe: DatePipe) { }

  user: any;
  userProfile: any;
  userAddress: any;
  freezePlan: any;
  Date = new Date();
  medicalMessage: string;
  allergyMessage: string;
  PlanAss: any;
  arrMenuUser: any;
  arrSubmitPlan: any;
  showLoadingSpinForSbMenu = true;

  addAddress(){
    this.userAddress.push({'address_name':'','address':''});
  }

  listUserData(){
    this.showLoadingSpinForSbMenu = true;
    this.utritionService.listUserData(this.user.id).subscribe(res=>{
      this.showLoadingSpinForSbMenu = false;
      this.userProfile = res.user;
      this.userAddress = res.user_address;
      this.freezePlan = res.freeze_plan;
      this.PlanAss = res.plan_ass;

      var currentDate = this.datePipe.transform(this.Date, 'yyyy-MM-dd');
      if(this.freezePlan != null){
        if(currentDate > this.freezePlan.to){
          this.freezePlan = null;
        }
      }

      if(this.userAddress.length == 0){
        this.addAddress();
      }

      // this.arrMenuUser = this.PlanAss[0].menuUser.week1;
      this.PlanAss.forEach(function (value){
        if(value.menu_submit == undefined){
          this.arrMenuUser = value.menuUser;
          // console.log(value.menuUser);
          // console.log(this.arrMenuUser );
        }else{
          this.arrSubmitPlan = value.menu_submit;
        }
      });

    });

  }

  ngOnInit(): void {
  }

}
