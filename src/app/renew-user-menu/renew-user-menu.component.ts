import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-renew-user-menu',
  templateUrl: './renew-user-menu.component.html',
  styleUrls: ['./renew-user-menu.component.css']
})
export class RenewUserMenuComponent implements OnInit {

  constructor(private router:Router,private utritionService: UtritionService, private toastr: ToastrService) { }

  renewPlanSpn = true;
  planDetails: any;
  public arrPlan: any;

  renewPlan(){
    if(this.planDetails == undefined){
      this.toastr.error('Please select a plan', 'Error', {timeOut: 5000,});
    }
    localStorage.setItem('planDetail',JSON.stringify(JSON.parse(this.planDetails)))
    //window.location.href='/plan-details';
    this.router.navigate(['/plan-details'])
  }

  ngOnInit(): void {

    this.utritionService.listPlan().subscribe(res=>{
      this.renewPlanSpn = false;
      this.arrPlan=res.data;
      console.log('res');
    },ero=>{
      console.log('ero');
    })

  }

}
