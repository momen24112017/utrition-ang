import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewUserMenuComponent } from './renew-user-menu.component';

describe('RenewUserMenuComponent', () => {
  let component: RenewUserMenuComponent;
  let fixture: ComponentFixture<RenewUserMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RenewUserMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RenewUserMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
