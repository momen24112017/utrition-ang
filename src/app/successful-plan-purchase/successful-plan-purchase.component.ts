import { Component, OnInit } from '@angular/core';
import { UtritionService } from '../services/utrition.service';

@Component({
  selector: 'app-successful-plan-purchase',
  templateUrl: './successful-plan-purchase.component.html',
  styleUrls: ['./successful-plan-purchase.component.css']
})
export class SuccessfulPlanPurchaseComponent implements OnInit {

  constructor( private utritionService: UtritionService) { }

  user: any;
  public status = 0;

  ngOnInit(): void {

    this.user = this.utritionService.getuserData();

    this.utritionService.checkBankPaymentStatus(this.user.id).subscribe(res => {
      if(res.status.code == 200){
        this.status = 1;
        console.log("successful");
      }else{
        this.status = 2;
      }
    });
  }

}
