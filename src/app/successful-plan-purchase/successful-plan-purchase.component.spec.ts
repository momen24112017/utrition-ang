import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessfulPlanPurchaseComponent } from './successful-plan-purchase.component';

describe('SuccessfulPlanPurchaseComponent', () => {
  let component: SuccessfulPlanPurchaseComponent;
  let fixture: ComponentFixture<SuccessfulPlanPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessfulPlanPurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessfulPlanPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
